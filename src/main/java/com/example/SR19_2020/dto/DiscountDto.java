package com.example.SR19_2020.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Positive;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class DiscountDto {
    @Positive(message = "This field is required")
    @Max(value = 99)
    private int discount;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date discountStart;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date discountEnd;

    @Positive(message = "This field is required")
    @Max(value = 99)


    //----------------------------------------------------

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(@Positive(message = "This field is required") @Max(value = 99) int discount) {
        this.discount = discount;
    }

    public Date getDiscountStart() {
        return discountStart;
    }

    public void setDiscountStart(Date discountStart) {
        this.discountStart = discountStart;
    }

    public Date getDiscountEnd() {
        return discountEnd;
    }

    public void setDiscountEnd(Date discountEnd) {
        this.discountEnd = discountEnd;
    }
}
