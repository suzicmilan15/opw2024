package com.example.SR19_2020.dto;

import jakarta.validation.constraints.*;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;

public class TripDto {
    @NotEmpty()
    private String destination;
    @NotEmpty(message = "This field is required")
    private String tripCategory;    // SUMMER, WINTER, LAST_MINUTE, NEW_YEAR
    @NotEmpty(message = "This field is required")
    private String transport;       // PLANE, BUS, PERSONAL
    @NotEmpty(message = "This field is required")
    private String accommodation;   // HOTEL, APARTMENT
    @Positive(message = "This field is required")
    private int numberOfNights;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateOfDeparture;
    @Positive(message = "This field is required")
    private double price;
    @Positive(message = "This field is required")
    private int totalSpots;

    //--------------------------------------------------------------------------

    public Integer getNumberOfNights() {
        return numberOfNights;
    }

    public void setNumberOfNights(int numberOfNights) {
        this.numberOfNights = numberOfNights;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getTripCategory() {
        return tripCategory;
    }

    public void setTripCategory(String tripCategory) {
        this.tripCategory = tripCategory;
    }

    public Date getDateOfDeparture() {
        return dateOfDeparture;
    }

    public void setDateOfDeparture(Date dateOfDeparture) {
        this.dateOfDeparture = dateOfDeparture;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getTotalSpots() {
        return totalSpots;
    }

    public void setTotalSpots(int totalSpots) {
        this.totalSpots = totalSpots;
    }

    public String getTransport() {
        return transport;
    }

    public void setTransport(String transport) {
        this.transport = transport;
    }

    public String getAccommodation() {
        return accommodation;
    }

    public void setAccommodation(String accommodation) {
        this.accommodation = accommodation;
    }

}
