package com.example.SR19_2020.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

public class ChangePasswordDto {
    @NotEmpty
    private String password;
    @Size(min = 6, message = "Password must be minimum 6 characters long! ")
    private String newPassword;
    @NotEmpty
    private String confirmPassword;

    //------------------------------------------------------


    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public @Size(min = 6, message = "Password must be minimum 6 characters long! ") String getPassword() {
        return password;
    }

    public void setPassword(@Size(min = 6, message = "Password must be minimum 6 characters long! ") String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
