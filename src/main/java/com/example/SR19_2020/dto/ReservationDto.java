package com.example.SR19_2020.dto;

import jakarta.validation.constraints.Positive;

public class ReservationDto {
    @Positive(message = "This field is required")
    private int numberOfPeople;



    //--------------------------------------------------------

    @Positive(message = "This field is required")
    public int getNumberOfPeople() {
        return numberOfPeople;
    }

    public void setNumberOfPeople(@Positive(message = "This field is required") int numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }
}
