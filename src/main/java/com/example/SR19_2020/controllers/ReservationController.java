package com.example.SR19_2020.controllers;

import com.example.SR19_2020.dto.ReservationDto;
import com.example.SR19_2020.dto.TripDto;
import com.example.SR19_2020.models.Reservation;
import com.example.SR19_2020.models.Trip;
import com.example.SR19_2020.models.User;
import com.example.SR19_2020.repositories.ReservationRepository;
import com.example.SR19_2020.repositories.TripRepository;
import com.example.SR19_2020.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;
import java.util.List;

@Controller
public class ReservationController {
    @Autowired
    private TripRepository tripRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ReservationRepository reservationRepository;

    @GetMapping("/create-reservation")
    public String showReservationPage(
            Model model,
            ReservationDto reservationDto,
            @RequestParam int id
    ) {

        try {
            Trip trip = tripRepository.findById(id).get();
            model.addAttribute("trip", trip);

            TripDto tripDto = new TripDto();
            tripDto.setDestination(trip.getDestination());
            tripDto.setTransport(trip.getTransport());
            tripDto.setAccommodation(trip.getAccommodation());
            tripDto.setTripCategory(trip.getTripCategory());
            tripDto.setDateOfDeparture(trip.getDateOfDeparture());
            tripDto.setPrice(trip.getPrice());
            tripDto.setTotalSpots(trip.getTotalSpots());
            tripDto.setNumberOfNights(trip.getNumberOfNights());

            model.addAttribute("reservationDto", reservationDto);
            model.addAttribute("tripDto", tripDto);
        }
        catch(Exception ex) {
            System.out.println("Exception: " + ex.getMessage());
            return "redirect:/";
        }

        return "/createReservation";
    }

    @PostMapping("/create-reservation")
    public String createReservation(Authentication auth,
                                    Model model,
                                    ReservationDto reservationDto,
                                    @RequestParam int id) {
        User user = userRepository.findByEmail(auth.getName());
        model.addAttribute("user", user);

        Trip trip = tripRepository.findById(id).get();
        model.addAttribute("trip", trip);

        Reservation reservation = new Reservation();
        reservation.setUserId(user.getId());
        reservation.setTripId(trip.getId());
        reservation.setDateOfReservation(new Date());
        reservation.setNumberOfPeople(reservationDto.getNumberOfPeople());
        if (Boolean.TRUE.equals(trip.getDiscountValid())){
            reservation.setTotalPrice(trip.getDiscountPrice() * reservationDto.getNumberOfPeople());
        }else {
            reservation.setTotalPrice(trip.getPrice() * reservationDto.getNumberOfPeople());
        }
        reservationRepository.save(reservation);

        trip.setSpotsLeft(trip.getSpotsLeft() - reservationDto.getNumberOfPeople());
        if (trip.getSpotsLeft() <= 0) {
            trip.setAvailable(false);
        }
        tripRepository.save(trip);

        return "redirect:/";
    }

    @GetMapping({"/reservations"})
    public String showReservations(Model model) {
        List<Reservation> reservations = reservationRepository.findAll();
        model.addAttribute("reservation", reservations);
        return "/reservations";
    }

    @GetMapping("/my-reservations")
    public String showUserReservations(Authentication auth, Model model) {
        User user = userRepository.findByEmail(auth.getName());  // Get the logged-in user
        List<Reservation> userReservations = reservationRepository.findByUserId(user.getId());  // Fetch reservations for the logged-in user

        model.addAttribute("reservations", userReservations);
        model.addAttribute("user", user);

        return "userReservations";  // Return a view that will display the user's reservations
    }


    @GetMapping("/deleteReservation")
    public String deleteReservation(
            @RequestParam int id,
            Model model
    ) {

        try {
            Reservation reservation = reservationRepository.findById(id).get();
            reservationRepository.delete(reservation);

            Trip trip = tripRepository.findById(reservation.getTripId()).get();
            model.addAttribute("trip", trip);
            trip.setSpotsLeft(trip.getSpotsLeft() + reservation.getNumberOfPeople());
            if (trip.getSpotsLeft() > 0) {
                trip.setAvailable(true);
            }
            tripRepository.save(trip);

        }
        catch (Exception ex) {
            System.out.println("Exception: " + ex.getMessage());
        }

        return "redirect:/reservations";
    }
}
