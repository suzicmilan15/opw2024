package com.example.SR19_2020.controllers;

import com.example.SR19_2020.dto.DiscountDto;
import com.example.SR19_2020.dto.TripDto;
import com.example.SR19_2020.models.Roles;
import com.example.SR19_2020.models.Trip;
import com.example.SR19_2020.models.User;
import com.example.SR19_2020.repositories.ReservationRepository;
import com.example.SR19_2020.repositories.TripRepository;
import com.example.SR19_2020.repositories.UserRepository;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;
import java.util.List;

@Controller
public class TripController {

    @Autowired
    private TripRepository tripRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ReservationRepository reservationRepository;


    @GetMapping({"", "/"})
    public String showTripList(Model model, Authentication auth) {
        List<Trip> trips;

        if (auth == null) {
            trips = tripRepository.findByAvailableTrue();
        } else {
            User user = userRepository.findByEmail(auth.getName());

            if (user.getRoles().equals(Roles.ADMIN)) {
                trips = tripRepository.findAll();
            } else {
                trips = tripRepository.findByAvailableTrue();
            }
        }

        model.addAttribute("trips", trips);

        boolean hasValidDiscount = trips.stream().anyMatch(Trip::getDiscountValid);
        model.addAttribute("hasValidDiscount", hasValidDiscount);

        return "home";
    }

    @GetMapping("/filter-trips")
    public String filterTrips(
            @RequestParam(required = false) Double minPrice,
            @RequestParam(required = false) Double maxPrice,
            Model model,
            Authentication auth) {

        List<Trip> trips;

        if (auth == null) {
            if (minPrice != null && maxPrice != null) {
                trips = tripRepository.findByPriceBetweenAndAvailableTrue(minPrice, maxPrice);
            } else if (minPrice != null) {
                trips = tripRepository.findByPriceGreaterThanEqualAndAvailableTrue(minPrice);
            } else if (maxPrice != null) {
                trips = tripRepository.findByPriceLessThanEqualAndAvailableTrue(maxPrice);
            } else {
                trips = tripRepository.findByAvailableTrue();
            }
        } else {
            // User is authenticated ADMIN
            User user = userRepository.findByEmail(auth.getName());

            if (user.getRoles().equals(Roles.ADMIN)) {
                if (minPrice != null && maxPrice != null) {
                    trips = tripRepository.findByPriceBetween(minPrice, maxPrice);
                } else if (minPrice != null) {
                    trips = tripRepository.findByPriceGreaterThanEqual(minPrice);
                } else if (maxPrice != null) {
                    trips = tripRepository.findByPriceLessThanEqual(maxPrice);
                } else {
                    trips = tripRepository.findAll(Sort.by(Sort.Direction.DESC, "id"));
                }
            } else {
                // Registered USER
                if (minPrice != null && maxPrice != null) {
                    trips = tripRepository.findByPriceBetweenAndAvailableTrue(minPrice, maxPrice);
                } else if (minPrice != null) {
                    trips = tripRepository.findByPriceGreaterThanEqualAndAvailableTrue(minPrice);
                } else if (maxPrice != null) {
                    trips = tripRepository.findByPriceLessThanEqualAndAvailableTrue(maxPrice);
                } else {
                    trips = tripRepository.findByAvailableTrue();
                }
            }
        }

        model.addAttribute("trips", trips);
        return "home";
    }



    @GetMapping("/create-trip")
    public String showCreatePage(Model model) {
        TripDto tripDto = new TripDto();
        model.addAttribute("tripDto", tripDto);
        return "createTrip";
    }

    @PostMapping("/create-trip")
    public String createTrip(
            @Valid @ModelAttribute TripDto tripDto,
            BindingResult result) {

        if (result.hasErrors()){
            return "createTrip";
        }

        try {
            Trip trip = new Trip();
            trip.setDestination(tripDto.getDestination());
            trip.setTransport(tripDto.getTransport());
            trip.setAccommodation(tripDto.getAccommodation());
            trip.setTripCategory(tripDto.getTripCategory());
            trip.setDateOfDeparture(tripDto.getDateOfDeparture());
            trip.setPrice(tripDto.getPrice());
            trip.setTotalSpots(tripDto.getTotalSpots());
            trip.setNumberOfNights(tripDto.getNumberOfNights());
            trip.setSpotsLeft(tripDto.getTotalSpots());
            trip.setDiscountValid(false);
            trip.setAvailable(true);

            tripRepository.save(trip);
        } catch (Exception ex){
            result.addError(
                    new FieldError("tripDto", "destination", ex.getMessage())
            );
        }

        return "redirect:/";
    }

    /*@GetMapping("/delete")
    public String deleteTrip(
            @RequestParam int id
    ) {

        try {
            Trip trip = tripRepository.findById(id).get();

            tripRepository.delete(trip);
        }
        catch (Exception ex) {
            System.out.println("Exception: " + ex.getMessage());
        }

        return "redirect:/";
    }*/

    @GetMapping("/delete")
    public String deleteTrip(
            @RequestParam int id,
            Model model
    ) {
        try {
            Trip trip = tripRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid trip Id:" + id));

            // Check if there are any reservations for this trip
            boolean hasReservations = reservationRepository.existsByTripId(id);
            if (hasReservations) {
                // If there are reservations, add an error message to the model
                model.addAttribute("error", "Cannot delete trip because there are existing reservations.");
                return "redirect:/?error=reservationExists";
            }

            // No reservations found, proceed with deletion
            tripRepository.delete(trip);
        } catch (Exception ex) {
            System.out.println("Exception: " + ex.getMessage());
            return "redirect:/?error=deleteFailed";
        }

        return "redirect:/";
    }


    @GetMapping("/edit")
    public String showEditPage(
            Model model,
            @RequestParam int id
    ) {

        try {
            Trip trip = tripRepository.findById(id).get();
            model.addAttribute("trip", trip);

            TripDto tripDto = new TripDto();
            tripDto.setDestination(trip.getDestination());
            tripDto.setTransport(trip.getTransport());
            tripDto.setAccommodation(trip.getAccommodation());
            tripDto.setTripCategory(trip.getTripCategory());
            tripDto.setDateOfDeparture(trip.getDateOfDeparture());
            tripDto.setPrice(trip.getPrice());
            tripDto.setTotalSpots(trip.getTotalSpots());
            tripDto.setNumberOfNights(trip.getNumberOfNights());

            model.addAttribute("tripDto", tripDto);
        }
        catch(Exception ex) {
            System.out.println("Exception: " + ex.getMessage());
            return "redirect:/";
        }

        return "/editTrip";
    }

    @PostMapping("/edit")
    public String updateTrip(
            Model model,
            @RequestParam int id,
            @Valid @ModelAttribute TripDto tripDto,
            BindingResult result
    ) {

        try {
            Trip trip = tripRepository.findById(id).get();
            model.addAttribute("trip", trip);

            if (result.hasErrors()) {
                return "/editTrip";
            }

            trip.setDestination(tripDto.getDestination());
            trip.setTransport(tripDto.getTransport());
            trip.setAccommodation(tripDto.getAccommodation());
            trip.setTripCategory(tripDto.getTripCategory());
            trip.setDateOfDeparture(tripDto.getDateOfDeparture());
            trip.setPrice(tripDto.getPrice());
            trip.setTotalSpots(tripDto.getTotalSpots());
            trip.setNumberOfNights(tripDto.getNumberOfNights());
            trip.setSpotsLeft(tripDto.getTotalSpots());

            tripRepository.save(trip);
        }
        catch(Exception ex) {
            System.out.println("Exception: " + ex.getMessage());
        }

        return "redirect:/";
    }

    @GetMapping("/discount")
    public String showDiscountPage(
            Model model,
            @RequestParam int id
    ) {
        try {
            Trip trip = tripRepository.findById(id).get();
            model.addAttribute("trip", trip);
            DiscountDto discountDto = new DiscountDto();
            TripDto tripDto = new TripDto();
            tripDto.setDestination(trip.getDestination());
            tripDto.setPrice(trip.getPrice());

            model.addAttribute("tripDto", tripDto);
            model.addAttribute("discountDto", discountDto);
        }
        catch(Exception ex) {
            System.out.println("Exception: " + ex.getMessage());
            return "redirect:/";
        }
        return "/discount";
    }

    @PostMapping("/discount")
    public String createDiscount(
            Model model,
            @RequestParam int id,
            @Valid @ModelAttribute
            DiscountDto discountDto,
            BindingResult result
    ) {

        try {
            Trip trip = tripRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid trip Id:" + id));
            model.addAttribute("trip", trip);

            if (result.hasErrors()) {
                return "/discount";
            }

            double discount = discountDto.getDiscount();
            double price = trip.getPrice();
            double discountPrice = price - (price * discount / 100);

            trip.setDiscountStart(discountDto.getDiscountStart());
            trip.setDiscountEnd(discountDto.getDiscountEnd());
            trip.setDiscount(discountDto.getDiscount());
            trip.setDiscountPrice(discountPrice);

            Date currentDate = new Date();
            if (currentDate.after(discountDto.getDiscountStart()) && currentDate.before(discountDto.getDiscountEnd())) {
                trip.setDiscountValid(true);
            } else {
                trip.setDiscountValid(false);
            }

            tripRepository.save(trip);
        }
        catch(Exception ex) {
            System.out.println("Exception: " + ex.getMessage());
        }

        return "redirect:/";
    }

}
