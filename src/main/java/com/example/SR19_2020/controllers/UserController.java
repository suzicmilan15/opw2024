package com.example.SR19_2020.controllers;

import com.example.SR19_2020.dto.ChangePasswordDto;
import com.example.SR19_2020.dto.EditDto;
import com.example.SR19_2020.dto.RegisterDto;
import com.example.SR19_2020.models.Roles;
import com.example.SR19_2020.models.User;
import com.example.SR19_2020.repositories.UserRepository;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@Controller
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/register")
    public String register(Model model){
        RegisterDto registerDto = new RegisterDto();
        model.addAttribute(registerDto);
        model.addAttribute("success", false);
        return "register";
    }

    @PostMapping("/register")
    public String register(
            Model model,
            @Valid @ModelAttribute RegisterDto registerDto,
            BindingResult result) {

        if (!registerDto.getPassword().equals(registerDto.getConfirmPassword())) {
            result.addError(
                    new FieldError("registerDto", "confirmPassword"
                            , "Password and Confirm Password do not match")
            );
        }
        User user = userRepository.findByEmail((registerDto.getEmail()));
        if (user != null){
            result.addError(
                    new FieldError("registerDto", "email",
                            "Email address is already used!")
            );
        }
        if (result.hasErrors()){
            return "register";
        }

        try {
            var bCryptEncoder = new BCryptPasswordEncoder();
            User newUser = new User();
            newUser.setFirstName(registerDto.getFirstName());
            newUser.setLastName(registerDto.getLastName());
            newUser.setEmail(registerDto.getEmail());
            newUser.setPassword(bCryptEncoder.encode(registerDto.getPassword()));
            newUser.setAddress(registerDto.getAddress());
            newUser.setPhoneNumber(registerDto.getPhoneNumber());
            newUser.setJmbg(registerDto.getJmbg());
            newUser.setDateOfRegistration(new Date());
            newUser.setDateOfBirth(registerDto.getDateOfBirth());
            newUser.setRoles(Roles.USER);

            userRepository.save(newUser);
            model.addAttribute("registerDto", new RegisterDto());
            model.addAttribute("success", true);

        }
        catch (Exception ex){
            result.addError(
                    new FieldError("registerDto", "firstName", ex.getMessage())
            );
        }
        return "register";
    }

    @GetMapping("/profile")
    public String profile(Authentication auth, Model model) {
        User user = userRepository.findByEmail(auth.getName()); //auth.getName je username
        model.addAttribute("user", user);

        return "profile";
    }

    @GetMapping("/editProfile")
    public String editProfileForm(Authentication auth,
                                  Model model) {

        try {
            User user = userRepository.findByEmail(auth.getName()); //auth.getName je username
            model.addAttribute("user", user);
            EditDto editDto = new EditDto();
            editDto.setFirstName(user.getFirstName());
            editDto.setLastName(user.getLastName());
            editDto.setAddress(user.getAddress());
            editDto.setPhoneNumber(user.getPhoneNumber());
            editDto.setJmbg(user.getJmbg());

            model.addAttribute("editDto", editDto);
        }

        catch(Exception ex) {
            System.out.println("Exception: " + ex.getMessage());
            return "redirect:/";
        }


        return "edit";
    }

    @PostMapping("/editProfile")
    public String updateUser(
            Model model,
            @Valid @ModelAttribute
            EditDto editDto,
            Authentication auth,
            BindingResult result){

        User user = userRepository.findByEmail(auth.getName());
        model.addAttribute("user", user);

        if (result.hasErrors()) {
            return "edit";
        }

        user.setFirstName(editDto.getFirstName());
        user.setLastName(editDto.getLastName());
        user.setAddress(editDto.getAddress());
        user.setPhoneNumber(editDto.getPhoneNumber());
        user.setJmbg(editDto.getJmbg());

        userRepository.save(user);

        return "redirect:/profile";
    }


    @GetMapping("/changePassword")
    public String changePasswordForm(Authentication auth,
                                     Model model,
                                     ChangePasswordDto changePasswordDto) {
        User user = userRepository.findByEmail(auth.getName());
        model.addAttribute("user", user);
        model.addAttribute("changePasswordDto", changePasswordDto);
        return "changePassword";
    }

    @PostMapping("/changePassword")
    public String changePassword(Authentication auth,
                                 Model model,
                                 @Valid @ModelAttribute ChangePasswordDto changePasswordDto,
                                 BindingResult result) {
        User user = userRepository.findByEmail(auth.getName());
        model.addAttribute("user", user);

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        // Check if the old password matches the current password
        if (!passwordEncoder.matches(changePasswordDto.getPassword(), user.getPassword())) {
            result.addError(new FieldError("changePasswordDto", "password", "Old password is incorrect"));
        }

        // Check if new password and confirm password match
        if (!changePasswordDto.getNewPassword().equals(changePasswordDto.getConfirmPassword())) {
            result.addError(new FieldError("changePasswordDto", "confirmPassword", "Password and Confirm Password do not match"));
        }

        // If there are any errors, return to the changePassword page
        if (result.hasErrors()) {
            return "changePassword";
        }

        // Update the user's password with the new password, properly encoded
        user.setPassword(passwordEncoder.encode(changePasswordDto.getNewPassword()));
        userRepository.save(user);

        return "redirect:/";
    }

}
