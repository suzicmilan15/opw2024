package com.example.SR19_2020.repositories;

import com.example.SR19_2020.models.Trip;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TripRepository extends JpaRepository<Trip, Integer> {

    List<Trip> findByPriceBetween(Double minPrice, Double maxPrice);

    List<Trip> findByPriceGreaterThanEqual(Double minPrice);

    List<Trip> findByPriceLessThanEqual(Double maxPrice);

    List<Trip> findByAvailableTrue();

    List<Trip> findByPriceBetweenAndAvailableTrue(Double minPrice, Double maxPrice);

    List<Trip> findByPriceGreaterThanEqualAndAvailableTrue(Double minPrice);

    List<Trip> findByPriceLessThanEqualAndAvailableTrue(Double maxPrice);

}
