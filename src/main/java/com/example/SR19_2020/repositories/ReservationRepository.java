package com.example.SR19_2020.repositories;

import com.example.SR19_2020.models.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Integer> {
    List<Reservation> findByUserId(int userId);
    boolean existsByTripId(int tripId);
}
