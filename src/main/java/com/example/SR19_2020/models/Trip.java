package com.example.SR19_2020.models;

import jakarta.persistence.*;

import java.util.Date;

@Entity
@Table(name = "trips")
public class Trip {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String destination;
    private String transport; // 0,1,2
    private String accommodation; //0,1
    private String tripCategory;
    private Date dateOfDeparture;
    private int numberOfNights;
    private double price;
    private double discountPrice;
    private int discount;               //unosi se % popusta
    private Date discountStart;         //unosi se datum od kog vazi akcija
    private Date discountEnd;           //unosi se datum do kog vazi akcija
    private Boolean discountValid;
    private int totalSpots;
    private int spotsLeft;
    private Boolean available;

    //----------------------------------------------------------------------------------------

    public Trip() {
    }

    public Trip(int id, String destination, String transport, String accommodation, String tripCategory, Date dateOfDeparture, int numberOfNights, double price, double discountPrice, int discount, Date discountStart, Date discountEnd, Boolean discountValid, int totalSpots, int spotsLeft, Boolean available) {
        this.id = id;
        this.destination = destination;
        this.transport = transport;
        this.accommodation = accommodation;
        this.tripCategory = tripCategory;
        this.dateOfDeparture = dateOfDeparture;
        this.numberOfNights = numberOfNights;
        this.price = price;
        this.discountPrice = discountPrice;
        this.discount = discount;
        this.discountStart = discountStart;
        this.discountEnd = discountEnd;
        this.discountValid = discountValid;
        this.totalSpots = totalSpots;
        this.spotsLeft = spotsLeft;
        this.available = available;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getTransport() {
        return transport;
    }

    public void setTransport(String transport) {
        this.transport = transport;
    }

    public String getAccommodation() {
        return accommodation;
    }

    public void setAccommodation(String accommodation) {
        this.accommodation = accommodation;
    }

    public String getTripCategory() {
        return tripCategory;
    }

    public void setTripCategory(String tripCategory) {
        this.tripCategory = tripCategory;
    }

    public Date getDateOfDeparture() {
        return dateOfDeparture;
    }

    public void setDateOfDeparture(Date dateOfDeparture) {
        this.dateOfDeparture = dateOfDeparture;
    }

    public int getNumberOfNights() {
        return numberOfNights;
    }

    public void setNumberOfNights(int numberOfNights) {
        this.numberOfNights = numberOfNights;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(double discountPrice) {
        this.discountPrice = discountPrice;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public Date getDiscountStart() {
        return discountStart;
    }

    public void setDiscountStart(Date discountStart) {
        this.discountStart = discountStart;
    }

    public Date getDiscountEnd() {
        return discountEnd;
    }

    public void setDiscountEnd(Date discountEnd) {
        this.discountEnd = discountEnd;
    }

    public Boolean getDiscountValid() {
        return discountValid;
    }

    public void setDiscountValid(Boolean discountValid) {
        this.discountValid = discountValid;
    }

    public int getTotalSpots() {
        return totalSpots;
    }

    public void setTotalSpots(int totalSpots) {
        this.totalSpots = totalSpots;
    }

    public int getSpotsLeft() {
        return spotsLeft;
    }

    public void setSpotsLeft(int spotsLeft) {
        this.spotsLeft = spotsLeft;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

}
