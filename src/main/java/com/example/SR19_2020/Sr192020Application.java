package com.example.SR19_2020;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sr192020Application {

	public static void main(String[] args) {
		SpringApplication.run(Sr192020Application.class, args);
	}

}
